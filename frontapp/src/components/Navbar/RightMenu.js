import React, { Fragment, useContext } from 'react'

import {
  useMediaQuery,
  useTheme,
} from '@material-ui/core'

import ProfileMenu from './ProfileMenu'

function RightMenu() {

  const theme = useTheme()
  const xsScreen = useMediaQuery(theme.breakpoints.only('xs'))


  return (
    <Fragment>
      <ProfileMenu />
    </Fragment>
  )
}

export default RightMenu
