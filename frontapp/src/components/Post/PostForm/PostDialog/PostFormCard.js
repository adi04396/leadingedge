import React, { lazy, useContext, useRef, useState } from 'react';
import { UIContext, UserContext } from '../../../../App';
import DialogLoading from '../../../UI/DialogLoading'
import {
  Button,
  TextField,
  Typography,
  CircularProgress,
  DialogActions,
  DialogContent,
  Dialog,
} from '@material-ui/core'

import useCreatePost from '../../hooks/useCreatePost';

export default function PostFormDialog() {
  const { uiState, uiDispatch } = useContext(UIContext)
  const [blob, setBlob] = useState(null)
  const [postImage, setPostImage] = useState(null)
  const [isImageCaptured, setIsImageCaptured] = useState(false)

  const [body, setBody] = useState({
    feelings: '',
    with: [],
    at: '',
    date: '',
  })

  const [postData, setPostData] = useState({
    privacy: 'Public',
    content: '',
  })

  const { userState } = useContext(UserContext)

  const handleContentChange = (e) => {
    setPostData({
      ...postData,
      content: e.target.value,
    })
  }

  const { handleSubmitPost, loading } = useCreatePost({
    postData,
    body,
    postImage,
    isImageCaptured,
    blob,
  })

  return (
    <div>
      <Typography
        style={{
          color: !uiState.darkMode ? 'grey' : null,
          padding: '8px',
          background: !uiState.darkMode ? 'rgb(240,242,245)' : null,
          borderRadius: '20px',

          cursor: 'pointer',
        }}
        onClick={() => uiDispatch({ type: 'SET_POST_MODEL', payload: true })}
      >
        What`s in your mind, {userState.currentUser.name} ?
      </Typography>

      {loading ? (
        <DialogLoading loading={loading} text="Uploading Post..." />
      ) : (
        <Dialog
          disableBackdropClick
          disableEscapeKeyDown
          fullWidth
          scroll="body"
          maxWidth="sm"
          open={uiState.postModel}
          onClose={() => uiDispatch({ type: 'SET_POST_MODEL', payload: false })}
          style={{ width: '100%' }}
        >
          <DialogContent>
            <TextField
              placeholder={`Whats in your mind ${userState.currentUser.name}`}
              multiline
              rows={8}
              value={postData.content}
              onChange={handleContentChange}
              style={{
                background: !uiState.darkMode ? '#fff' : null,
                border: 'none',
                width: '100%',
              }}
            />
          </DialogContent>
          <DialogActions>
            <Button
              disabled={loading}
              onClick={handleSubmitPost}
              variant="contained"
              color="primary"
              style={{ width: '100%' }}
            >
              {loading ? (
                <CircularProgress
                  variant="indeterminate"
                  size={25}
                  style={{ color: '#fff' }}
                />
              ) : (
                ' Create Post'
              )}
            </Button>
           
          </DialogActions>
        </Dialog>
      )}
    </div>
  )
}
