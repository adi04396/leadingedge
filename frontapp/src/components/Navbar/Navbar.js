import React, { Fragment, useContext } from 'react'
import { UIContext, UserContext } from '../../App'
import useStyles from './styles'
import RightMenu from './RightMenu'

import {
  AppBar,
  Toolbar,
  useTheme,
} from '@material-ui/core'

function Navbar() {
  const { uiState, uiDispatch } = useContext(UIContext)
  const classes = useStyles()
  const theme = useTheme()

  return (
    <Fragment>
      <AppBar
        color="default"
        style={{
          backgroundColor: !uiState.darkMode ? 'white' : 'rgb(36,37,38)',
          color: !uiState.darkMode ? 'blue' : null,
        }}
        className={classes.root}
        style={{zIndex:"10000"}}
        elevation={1}
      
      >
        <Toolbar>
         
          <div className={classes.rightMenu}>
            <RightMenu />
          </div>
        </Toolbar>
      </AppBar>
    </Fragment>
  )
}

export default Navbar
