import React, { useContext, useEffect } from 'react'
import { PostContext, UIContext, UserContext } from '../App'
import {
  useTheme,
  useMediaQuery,
} from '@material-ui/core'
import WritePostCard from '../components/Post/PostForm/WritePostCard'

import Posts from '../components/Post/Posts'
import useFetchPost from '../hooks/useFetchPost'

function Home() {
  const { uiState, uiDispatch } = useContext(UIContext)
  const { userState } = useContext(UserContext)
  const {  postState } = useContext(PostContext)
  const theme = useTheme()
  const match = useMediaQuery(theme.breakpoints.between(960, 1400))

  const { fetchPosts } = useFetchPost()
  useEffect(() => {
    uiDispatch({ type: 'SET_NAV_MENU', payload: true })
    uiDispatch({ type: 'SET_DRAWER', payload: false })

    async function loadPosts() {
      await fetchPosts()
    }

    loadPosts()

    return () => {
      uiDispatch({ type: 'SET_NAV_MENU', payload: false })
      uiDispatch({ type: 'SET_DRAWER', payload: false })
    }
  }, [])

  return (
    <div>

      <div
        style={{
          maxWidth: uiState.mdScreen ? (match ? '45vw' : '38vw') : '100vw',
          margin: 'auto',
          paddingTop: '100px',
          paddingBottom: '100px',
          minHeight: '100vh',
        }}
      >
        <WritePostCard user={userState.currentUser} />

        <Posts posts={postState.posts} />
      </div>
    </div>
  )
}

export default Home
