const router = require('express').Router()
const {
  me,
  fetchUserById,
} = require('../controllers/User/FetchUser')

const authRequired = require('../middleware/AuthRequired')

router.get('/me', authRequired, me)
router.get('/:user_id', authRequired, fetchUserById)


module.exports = router
