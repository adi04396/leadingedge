import React, { useContext, useState } from 'react'
import { UserContext, UIContext } from '../../App'
import { LogoutUser } from '../../services/AuthService'
import { Link, useHistory } from 'react-router-dom'
import {
  Menu,
  IconButton,
  List,
  ListItem,
  ListItemText,
  Typography,
  useTheme,
  useMediaQuery,
} from '@material-ui/core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faChevronDown } from '@fortawesome/free-solid-svg-icons'

function ProfileMenu() {
  const history = useHistory()
  const { userState, userDispatch } = useContext(UserContext)
  const { uiState, uiDispatch } = useContext(UIContext)
  const [profileMenu, setProfileMenu] = useState(null)

  const theme = useTheme()
  const xsScreen = useMediaQuery(theme.breakpoints.only('xs'))

  const handleUserLogout = () => {
    LogoutUser()
      .then((res) => {
        if (res.data) {
          userDispatch({
            type: 'ADD_RECENT_ACCOUNT',
            payload: res.data.account,
          })
          userDispatch({ type: 'LOGOUT_USER' })
          history.push('/')
        }
        if (res.error) {
          uiDispatch({
            type: 'SET_MESSAGE',
            payload: {
              color: 'error',
              display: true,
              text: res.data.error,
            },
          })
        }
      })
      .catch((err) => {
        console.log(err)
      })
  }
  return (
    <div>
      <IconButton
        style={{
          marginLeft: xsScreen ? '4px' : '8px',
          color: !uiState.darkMode ? 'dark' : null,
          backgroundColor: !uiState.darkMode ? '#F0F2F5' : null,
        }}
        onClick={(e) => setProfileMenu(e.currentTarget)}
      >
        <FontAwesomeIcon icon={faChevronDown} size={xsScreen ? 'xs' : 'sm'} />
      </IconButton>

      <Menu
        id="profile-menu"
        anchorEl={profileMenu}
        open={Boolean(profileMenu)}
        onClose={() => setProfileMenu(null)}
        style={{ marginTop: '50px' }}
        elevation={7}
      >
        <List>
          <ListItem button onClick={handleUserLogout}>
            <ListItemText>
              <Typography style={{ fontSize: '15px' }}> Logout</Typography>
            </ListItemText>
          </ListItem>
        </List>
      </Menu>
    </div>
  )
}

export default ProfileMenu
